<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpgameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stgame', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->unsigned()->default(0);
          $table->string('username');
          $table->string('team');
          $table->string('week');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stgame');
    }
}
