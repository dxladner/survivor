<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.meta')
    @include('layouts.css')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

  </head>
  <body role="document">
        @include('layouts.navmenu')
      <div class="container" role="main">
        @yield('content')
      </div> <!-- /container -->


      @include('layouts.js')

  </body>
</html>
