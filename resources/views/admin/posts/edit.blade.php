@extends('admin.layouts.master')

@section('title', 'Edit a Post')

@section('content')

<!-- ******************   content ******************************-->
    <div class="row"><!-- row -->
      <div class="col-md-12">
          <!-- error messages -->
          @foreach ($errors->all() as $error)
              <p class="alert alert-danger">{{ $error }}</p>
          @endforeach
          <!-- status messages -->
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Edit a Post</h3>
            </div>
            <div class="panel-body">
              <form class="form-horizontal" method="post" action="{{ url('/') }}/admin/posts/{{ $post->id }}/edit">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                  <input required="required" value="{!! $post->title !!}" placeholder="Enter title here" type="text" id="title" name="title" class="form-control" />
                </div>
                <div class="form-group">
                  <textarea name='body' id="body" class="form-control">{!! $post->body !!}</textarea>
                </div>
                <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
                <input type="submit" name='save' class="btn btn-default" value = "Save Draft" />
              </form>
            </div><!-- end panel body -->
          </div><!-- end panel -->
        </div>
    </div>

@endsection
