@extends('admin.layouts.master')
@section('title', 'Edit a Player')
@section('content')


<div class="row"><!-- row -->


   <!-- ******************   content ******************************-->
        <div class="col-md-12">
          <h1 class="page-header">{{ Auth::user()->username }}'s Dashboard</h1>
           @if (Auth::user()->is_admin != true)
               <h1>Access Denied! This page is only for Administrators</h1>
           @else
               <h1 class="page-header">Edit Players</h1>

               <form method="POST" action="">

               <!-- error messages -->
               @foreach ($errors->all() as $error)
                   <p class="alert alert-danger">{{ $error }}</p>
               @endforeach
               <!-- status messages -->
               @if (session('status'))
                   <div class="alert alert-success">
                       {{ session('status') }}
                   </div>
               @endif
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="form-group">
                 <label for="name">Player Name</label>
                 <input type="text" class="form-control" id="name" name="name" value="{{ $player->name }}">
               </div>

               <div class="form-group">
                 <label for="team">Player Team</label>
                 <select class="form-control" id="team" name="team">
                       <option value="{!! $player->team !!}">{!! $player->team !!}</option>
                       @foreach(config('constants.TEAMS') as $value => $team)
                         <option value="{{ $value }}">
                           {{ $team }}
                         </option>
                       @endforeach
                   </select>
               </div>
               <div class="form-group">
                   <label for="position">Player Position</label>
                   <select class="form-control" id="position" name="position">
                       <option value="{!! $player->position !!}">{!! $player->position !!}</option>
                       @foreach(config('constants.PLAYERPOSITIONS') as $value => $position)
                         <option value="{{ $value }}">
                           {{ $position }}
                         </option>
                       @endforeach
                   </select>
                 </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
              </form>

        
       </div>
     </div><!-- row -->

		    @endif
@endsection
