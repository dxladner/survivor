@extends('admin.layouts.master')

@section('title', 'Players')

@section('content')

	<div class="row">

				<!-- ******************   content ******************************-->
		        <div class="col-md-12">
		        <h1 class="page-header">{{ Auth::user()->username }}'s Dashboard</h1>
		        @if (Auth::user()->is_admin != true)
			        <h1>Access Denied! This page is only for Administrators</h1>
			    @else
		          <h1 class="page-header">Players</h1>
		          <hr>
		          <h5>Enter Players</h5>

		          <form method="POST" action="">

			        <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
	                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
		            <div class="form-group">
		              <label for="name">Player Name</label>
		              <input type="text" class="form-control" id="name" name="name">
		            </div>

		            <div class="form-group">
		              <label for="team">Player Team</label>
		              <select class="form-control" id="team" name="team">
			            	@foreach(config('constants.TEAMS') as $value => $team)
											<option value="{{ $value }}">
												{{ $team }}
											</option>
										@endforeach
		            	</select>
		            </div>
		            <div class="form-group">
		            	<label for="position">Player Position</label>
		            	<select class="form-control" id="position" name="position">
										@foreach(config('constants.PLAYERPOSITIONS') as $value => $position)
											<option value="{{ $value }}">
												{{ $position }}
											</option>
										@endforeach
		            	</select>
		            </div>
		            <button type="submit" class="btn btn-primary">Submit</button>
		          </form>


		          <h2 class="sub-header">Players Section</h2>
		          <div class="row">
		          	<hr>
		          	<div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Players</h3>

                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">Quarterbacks</a></li>
                            <li><a href="#tab2" data-toggle="tab">Runningbacks One</a></li>
                            <li><a href="#tab6" data-toggle="tab">Runningbacks Two</a></li>
                            <li><a href="#tab3" data-toggle="tab">Widereceivers One</a></li>
                            <li><a href="#tab7" data-toggle="tab">Widereceivers Two</a></li>
                            <li><a href="#tab4" data-toggle="tab">Tight Ends</a></li>
                            <li><a href="#tab5" data-toggle="tab">Defense</a></li>
                        </ul>

                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1"><!-- tab1 -->
									<h5>Quarterbacks</h5>
					          		<table class="table table-striped">
							            <thead>
							                <tr>
							                  <th>#</th>
							                  <th>Name</th>
							                  <th>Team</th>
							                  <th>Position</th>
							                  <th>Edit</th>
							                  <th>Delete</th>
							                </tr>
							            </thead>
							            <tbody>
							            @foreach($quarterbacks as $quarterback)
							                <tr>
							                  <td>{{ $quarterback->id }}</td>
							                  <td>{{ $quarterback->name }}</td>
							                  <td>{{ $quarterback->team }}</td>
							                  <td>{{ ucfirst(trans($quarterback->position)) }}</td>
							                  <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $quarterback->id }}/edit">Edit</a></td>
							                  <td>
							                  	<form method="post" action="{{ url('/') }}/admin/player/{{ $quarterback->id }}/delete" >
									            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
									                    <div class="form-group">
									                        <div>
									                            <button type="submit" class="btn btn-danger">Delete</button>
									                        </div>
									                    </div>
											            </form>
											          </td>
							                </tr>
							             @endforeach
							            </tbody>
						            </table>

						</div><!-- tab1 -->
    					<div class="tab-pane" id="tab2"><!-- tab2 -->
								<h5>Runningback One</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						            @foreach($runningbacks_one as $runningback_one)
						                <tr>
						                  <td>{{ $runningback_one->id }}</td>
						                  <td>{{ $runningback_one->name }}</td>
						                  <td>{{ $runningback_one->team }}</td>
						                  <td>{{ ucfirst(trans($runningback_one->position)) }}</td>
						                   <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $runningback_one->id }}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="{{ url('/') }}/admin/player/{{ $runningback_one->id }}/delete" >
										            <input type="hidden" name="_token" value="{{ csrf_token() }}">
										                    <div class="form-group">
										                        <div>
										                            <button type="submit" class="btn btn-danger">Delete</button>
										                        </div>
										                    </div>
										            </form>
										           </td>
						                </tr>
						             @endforeach
						            </tbody>
					            </table>


						</div><!-- tab2 -->
						<div class="tab-pane" id="tab6"><!-- tab6 -->
								<h5>Runningback Two</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						            @foreach($runningbacks_two as $runningback_two)
						                <tr>
						                  <td>{{ $runningback_two->id }}</td>
						                  <td>{{ $runningback_two->name }}</td>
						                  <td>{{ $runningback_two->team }}</td>
						                  <td>{{ ucfirst(trans($runningback_two->position)) }}</td>
						                   <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $runningback_two->id }}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="{{ url('/') }}/admin/player/{{ $runningback_two->id }}/delete" >
										            <input type="hidden" name="_token" value="{{ csrf_token() }}">
										                    <div class="form-group">
										                        <div>
										                            <button type="submit" class="btn btn-danger">Delete</button>
										                        </div>
										                    </div>
										            </form>
										           </td>
						                </tr>
						             @endforeach
						            </tbody>
					            </table>


						</div><!-- tab6 -->
						<div class="tab-pane" id="tab3"><!-- tab3 -->
								<h5>Widereceivers One</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						               @foreach($widereceivers_one as $widereceiver_one)
						                <tr>
						                  <td>{{ $widereceiver_one->id }}</td>
						                  <td>{{ $widereceiver_one->name }}</td>
						                  <td>{{ $widereceiver_one->team }}</td>
						                  <td>{{ ucfirst(trans($widereceiver_one->position)) }}</td>
						                   <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $widereceiver_one->id }}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="{{ url('/') }}/admin/player/{{ $widereceiver_one->id }}/delete" >
										            <input type="hidden" name="_token" value="{{ csrf_token() }}">
										                    <div class="form-group">
										                        <div>
										                            <button type="submit" class="btn btn-danger">Delete</button>
										                        </div>
										                    </div>
										            </form>
						                   </td>
						                </tr>
						             @endforeach
						            </tbody>
					            </table>


						</div><!-- tab3 -->
						<div class="tab-pane" id="tab7"><!-- tab7 -->
								<h5>Widereceivers Two</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						               @foreach($widereceivers_two as $widereceiver_two)
						                <tr>
						                  <td>{{ $widereceiver_two->id }}</td>
						                  <td>{{ $widereceiver_two->name }}</td>
						                  <td>{{ $widereceiver_two->team }}</td>
						                  <td>{{ ucfirst(trans($widereceiver_two->position)) }}</td>
						                   <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $widereceiver_two->id }}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="{{ url('/') }}/admin/player/{{ $widereceiver_two->id }}/delete" >
										            <input type="hidden" name="_token" value="{{ csrf_token() }}">
										                    <div class="form-group">
										                        <div>
										                            <button type="submit" class="btn btn-danger">Delete</button>
										                        </div>
										                    </div>
										            </form>
						                   </td>
						                </tr>
						             @endforeach
						            </tbody>
					            </table>


						</div><!-- tab7 -->
						<div class="tab-pane" id="tab4"><!-- tab4 -->
								<h5>Tight Ends</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						               @foreach($tightends as $tightend)
						                <tr>
						                  <td>{{ $tightend->id }}</td>
						                  <td>{{ $tightend->name }}</td>
						                  <td>{{ $tightend->team }}</td>
						                  <td>{{ ucfirst(trans($tightend->position)) }}</td>
						                   <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $tightend->id }}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="{{ url('/') }}/admin/player/{{ $tightend->id }}/delete" >
										            <input type="hidden" name="_token" value="{{ csrf_token() }}">
										                    <div class="form-group">
										                        <div>
										                            <button type="submit" class="btn btn-danger">Delete</button>
										                        </div>
										                    </div>
										            </form>
									       </td>
						                </tr>
						             @endforeach
						            </tbody>
					            </table>


						</div><!-- tab4 -->
						<div class="tab-pane" id="tab5"><!-- tab5 -->

							<h5>Defense</h5>
				          		<table class="table table-striped">
						            <thead>
						                <tr>
						                  <th>#</th>
						                  <th>Name</th>
						                  <th>Team</th>
						                  <th>Position</th>
						                </tr>
						            </thead>
						            <tbody>
						                @foreach($defenses as $defense)
						                <tr>
						                  <td>{{ $defense->id }}</td>
						                  <td>{{ $defense->name }}</td>
						                  <td>{{ $defense->team }}</td>
						                  <td>{{ ucfirst(trans($defense->position)) }}</td>
						                   <td><a class="btn btn-warning" href="{{ url('/') }}/admin/player/{{ $defense->id }}/edit">Edit</a></td>
						                   <td>
						                   	<form method="post" action="{{ url('/') }}/admin/player/{{ $defense->id }}/delete" >
										            <input type="hidden" name="_token" value="{{ csrf_token() }}">
										                    <div class="form-group">
										                        <div>
										                            <button type="submit" class="btn btn-danger">Delete</button>
										                        </div>
										                    </div>
										            </form>
										           </td>
						                </tr>
						             @endforeach
						            </tbody>
					            </table>

										</div><!-- tab5 -->
                  </div>
                </div>
            </div>
		        </div>
		      </div><!-- row -->

		    @endif
@endsection
