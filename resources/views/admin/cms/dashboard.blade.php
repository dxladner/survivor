@extends('admin.layouts.master')

@section('title', 'CMS Dashboard')

@section('content')

<!-- ******************   content ******************************-->
    <div class="row"><!-- row -->
      <div class="col-md-12">
        <h2><i class="fa fa-newspaper-o"></i> CMS Dashboard</h2>
          <!-- error messages -->
          @foreach ($errors->all() as $error)
              <p class="alert alert-danger">{{ $error }}</p>
          @endforeach
          <!-- status messages -->
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Create a Post</h3>
            </div>
            <div class="panel-body">
              <form method="post" action="{{ url('/') }}/admin/posts">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                  <input required="required" value="{{ old('title') }}" placeholder="Enter title here" type="text" name = "title"class="form-control" />
                </div>
                <div class="form-group">
                  <textarea name='body'class="form-control">{{ old('body') }}</textarea>
                </div>
                <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
                <input type="submit" name='save' class="btn btn-default" value = "Save Draft" />
              </form>
            </div><!-- end panel body -->
          </div><!-- end panel -->
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <h2 class="sub-header">Posts</h2>
        <div class="table-responsive">
          <table class="table table-striped">
              <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Created</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
              </thead>
              <tbody>
              @if ($posts->isEmpty())
                  <p> There is no posts.</p>
              @else

                  @foreach($posts as $post)
                  <tr>
                    <td>{{ $post->id }}</td>
                    <td><strong>{!! $post->title !!}</strong></td>
                    <td>{!! $post->created_at->format('M d,Y') !!}</td>
                    <td>
                        <a href="{{ url('/') }}/admin/posts/{{ $post->id }}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    </td>
                    <td>
                      <form method="post" action="{{ url('/') }}/admin/posts/{{ $post->id }}/delete" >
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group">
                              <div>
                                  <button type="submit" class="btn btn-danger">Delete</button>
                              </div>
                          </div>
                      </form>
                    </td>
              </tr>
                  @endforeach

              @endif

              </tbody>
          </table>
      </div>
    </div>
  </div><!-- row -->

@endsection
