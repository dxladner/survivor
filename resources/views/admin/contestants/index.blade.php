@extends('admin.layouts.master')

@section('title', 'Contestants')

@section('content')

<div class="col-lg-10 col-lg-offset-1">

<h2><i class="fa fa-users"></i> Contestants Administration</h2>
    <!-- error messages -->
    @foreach ($errors->all() as $error)
        <p class="alert alert-danger">{{ $error }}</p>
    @endforeach
    <!-- status messages -->
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="table-responsive">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Date/Time Added</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->getFullName() }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                    <td>
                        <a href="{{ url('/') }}/admin/contestants/{{ $user->id }}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    </td>
                    <td>
                      <form method="post" action="{{ url('/') }}/admin/contestants/{{ $user->id }}/delete" >
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group">
                              <div>
                                  <button type="submit" class="btn btn-danger">Delete</button>
                              </div>
                          </div>
                      </form>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

    <a href="{{ url('/' )}}/admin/contestants/show" class="btn btn-success">Add Contestant</a>

</div>


@endsection
