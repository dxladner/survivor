@extends('admin.layouts.master')

@section('title', 'Edit a Contestant')

@section('content')


<div class="row"><!-- row -->

      <div class="container col-md-10 col-md-offset-1">
          <div class="well well bs-component">

              <form class="form-horizontal" method="post">

                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                  <fieldset>
                      <legend>Edit User</legend>
                      <div class="form-group">
                          <label for="first_name" class="col-lg-2 control-label">First Name</label>
                          <div class="col-lg-10">
                              <input type="text" class="form-control" id="first_name" name="first_name" value="{!! $user->first_name !!}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="last_name" class="col-lg-2 control-label">Last Name</label>
                          <div class="col-lg-10">
                              <input type="text" class="form-control" id="last_name" name="last_name" value="{!! $user->last_name !!}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="username" class="col-lg-2 control-label">User Name</label>
                          <div class="col-lg-10">
                              <input type="text" class="form-control" id="username" name="username" value="{!! $user->username !!}">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="email" class="col-lg-2 control-label">Email</label>
                          <div class="col-lg-10">
                              <input type="text" class="form-control" id="email" name="email" value="{!! $user->email !!}">
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-lg-10 col-lg-offset-2">
                              <a href="{!! action('UserController@show', $user->id) !!}" class="btn btn-success">Cancel</a>
                              <button type="submit" class="btn btn-primary">Update</button>
                          </div>
                      </div>
                  </fieldset>
              </form>
          </div>
        </div>
     </div><!-- row -->

@endsection
