<!-- jQuery -->
<script src="{{ url('/') }}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ url('/') }}/js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ url('/') }}/js/plugins/morris/raphael.min.js"></script>
<script src="{{ url('/') }}/js/plugins/morris/morris.min.js"></script>
<script src="{{ url('/') }}/js/plugins/morris/morris-data.js"></script>

<script type="text/javascript" src="{{ url('/') }}/js/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
    tinymce.init({
      selector : "textarea",
      plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
      toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
  </script>
