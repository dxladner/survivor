<!DOCTYPE html>
<html lang="en">
  <head>

    @include('admin.layouts.meta')
    @include('admin.layouts.css')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

  </head>
  <body>

      <div id="wrapper">
          @include('admin.layouts.navmenu')

          <div id="page-wrapper">

              <div class="container-fluid">

                  @yield('content')

              </div>
              <!-- /.container-fluid -->

          </div>
          <!-- /#page-wrapper -->

      </div>
      <!-- /#wrapper -->

      @include('admin.layouts.js')

  </body>

  </html>
