<!-- Styles -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Custom CSS -->
<link href="{{ url('/') }}/css/sb-admin.css" rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="{{ url('/') }}/css/plugins/morris.css" rel="stylesheet">
<link href="{{ url('/') }}/css/style.css" rel="stylesheet">
