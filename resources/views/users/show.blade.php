@extends('layouts.master')

@section('content')

    <div class="container col-md-8 col-md-offset-2">
        <div class="well well bs-component">
            <div class="content">
                <h2 class="header">{!! $user->name !!}</h2>
                <p> {!! $user->email !!} </p>
            </div>

            @if (Auth::check())
            <table>
                <tr>
                    <td>
                        <a href="{!! action('UserController@index') !!}" class="btn btn-primary">Cancel</a>
                    </td>
                    <td>
                    &nbsp;
                    </td>
                    <td>
                        <a href="{{ URL::to('/users/edit/' . $user->id) }}" class="btn btn-warning">Edit User</a>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>
                        <form id="deleteForm" method="post" action="{!! action('UserController@destroy', $user->id) !!}" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            </table>
            @endif
            <div class="clearfix"></div>
        </div>

    </div>


@endsection
