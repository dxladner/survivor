
<!-- ST GAME DASHBOARD WIDGET -->

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">{{ Auth::user()->username }} Dashboard: ST Game</h3>
        </div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/sfgame/saveusergame') }}">
                  {{ csrf_field() }}

                  <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->username }}">
                  <div class="form-group{{ $errors->has('team') ? ' has-error' : '' }}">
                      <label for="team" class="col-md-4 control-label">Team</label>

                      <div class="col-md-6">
                        <select class="form-control" id="team" name="team">
                          @foreach(config('constants.TEAMS') as $value => $team)
                            <option value="{{ $value }}">
                              {{ $team }}
                            </option>
                          @endforeach
                        </select>

                          @if ($errors->has('team'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('team') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-6 col-md-offset-4">
                          <button type="submit" class="btn btn-primary">
                              ENTER YOUR TEAM
                          </button>
                      </div>
                  </div>
              </form>
        </div>
      </div>
