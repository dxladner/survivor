<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    protected $fillable = ['author_id', 'title', 'body', 'slug', 'active'];

    public function post()
    {
      return $this->belongsTo('App\User');
    }

    public function getTitle()
    {
      return $this->title;
    }

    public function getPostDate($value)
    {
      return Carbon::parse($value)->format('m-d-Y');
    }

}
