<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function stgame()
    {
      return $this->hasMany('App\STGame');
    }
    /**
  	 * Get the user's full name by concatenating the first and last names
  	 *
  	 * @return string
  	 */
  	public function getFullName()
  	{
  		return $this->first_name . ' ' . $this->last_name;
  	}

    public function isAdmin()
    {
      return Auth::user()->is_admin == 1;
    }
}
