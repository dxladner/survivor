<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class STGame extends Model
{
    protected $fillable = ['user_id', 'username', 'team', 'week'];

    public function stgame()
    {
        return $this->belongsTo('App\User');
    }
}
