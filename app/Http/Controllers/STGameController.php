<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\STGame;
use Auth;

class STGameController extends Controller
{
    public function usergame()
    {
        $user_id = Auth::id();
        $usergame = App\STGame::find(1);

        $test = $usergame->team;

    }

    public function saveusergame(Request $request)
    {
        $this->validate($request, [
           'user_id'  => 'required',
           'username' => 'required',
           'team' 	  => 'required',
           'week'     => 'required',
        ]);

       $stgame = STGame::create([
         'user_id'  => $request->user_id,
         'username' => $request->username,
         'team'     => $request->team,
         'week'     => $request->week,
       ]);

       $stgame->save();

       return view('users.dashboard')->with('success', 'You selected ' . $stgame->team . ' for this week.');

    }
}
