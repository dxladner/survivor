<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class ContestantsController extends Controller
{
    /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function index()
    {
        $users = User::all();
        return view('admin.contestants.index', compact('users'));
    }

    public function show()
    {
        return view('admin.contestants.create');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
          'first_name'  => 'required|max:255',
          'last_name'   => 'required|max:255',
          'username'    => 'required|max:255',
          'email'       => 'required|email|max:255|unique:users',
          'password'    => 'required|min:6|confirmed',
       ]);

       $contestants = User::create([
         'first_name'  => $request['first_name'],
         'last_name'   => $request['last_name'],
         'username'    => $request['username'],
         'email'       => $request['email'],
         'password'    => bcrypt($request['password']),
         'is_admin'    => 0,
       ]);

       $contestants->save();

       return redirect('/admin/contestants/')->with('status', 'Contestant successfully created!');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.contestants.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update($id, Request $request)
     {
         $user = User::findOrFail($id);
         $user->first_name  = $request->get('first_name');
         $user->last_name   = $request->get('last_name');
         $user->username    = $request->get('username');
         $user->email       = $request->get('email');
         $user->save();
         return redirect('/admin/contestants/')->with('status', 'The Contestants '. $user->username .' has been updated!');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $user = User::findOrFail($id);
        $user->username    = $request->get('username');
        $user->delete();
        return redirect('/admin/contestants/')->with('status', 'The Contestant ' . $user->username . ' has been deleted!');
    }
}
