<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use DB;

class CMSController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('admin.cms.dashboard', compact('posts'));
    }
}
