<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Player;

class PlayersController extends Controller
{
    public function index()
    {
        if (Auth::check())
        {
          $quarterbacks = Player::where('position', '=', 'quarterback')->get();
          $runningbacks_one = Player::where('position', '=', 'runningback_one')->get();
          $runningbacks_two = Player::where('position', '=', 'runningback_two')->get();
          $widereceivers_one = Player::where('position', '=', 'widereceiver_one')->get();
          $widereceivers_two = Player::where('position', '=', 'widereceiver_two')->get();
          $tightends = Player::where('position', '=', 'tightend')->get();
          $defenses = Player::where('position', '=', 'defense')->get();

          //dd($quarterbacks);
          return view('admin.players.index', compact('quarterbacks', 'runningbacks_one', 'runningbacks_two', 'widereceivers_one', 'widereceivers_two', 'tightends', 'defenses'));
        }
        else
        {
            return Redirect::to('http://localhost/survivor/public/');
        }
    }

    public function store(Request $request)
    {
         $this->validate($request, [
	        'name' 	   => 'required',
	        'team' 	   => 'required',
	        'position' => 'required',
	      ]);

	      $player = Player::create([
          'name' 	   => $request->name,
	        'team' 	   => $request->team,
	        'position' => $request->position,
	      ]);

	      $player->save();

	      return view('admin.players.index')->with('success', 'Player successfully created!');
    }

    public function edit($id)
    {
        $player = Player::findOrFail($id);
        return view('admin.players.edit', compact('player'));
    }

    public function update($id, Request $request)
    {
        $player = Player::findOrFail($id);
        $player->name = $request->get('name');
        $player->team = $request->get('team');
        $player->position = $request->get('position');

        $player->save();
        return redirect('/admin/players/')->with('status', 'The player '.$player->name.' has been updated!');
    }

    public function destroy($id)
    {
        $player = Player::findOrFail($id);
        $player->delete();
        return redirect('/admin/players')->with('status', 'The player '.$player->name.' has been deleted!');
    }

}
