<?php
return array (

/*
|--------------------------------------------------------------------------
| Site global variables
|--------------------------------------------------------------------------
|
| This are global variables that you can access from controller, models or views
|
*/

    'TEAMS' => array(
          'Arizona Cardinals'     => "Arizona Cardinals",
          'Atlanta Falcons'       => "Atlanta Falcons",
          'Baltimore Ravens'      => "Baltimore Ravens",
          'Buffalo Bills'         => "Buffalo Bills",
          'Carolina Panthers'     => "Carolina Panthers",
          'Chicago Bears'         => "Chicago Bears",
          'Cincinnati Bengals'    => "Cincinnati Bengals",
          'Cleveland Browns'      => "Cleveland Browns",
          'Dallas Cowboys'        => "Dallas Cowboys",
          'Denver Broncos'        => "Denver Broncos",
          'Detroit Lions'         => "Detroit Lions",
          'Green Bay Packers'     => "Green Bay Packers",
          'Houston Texans'        => "Houston Texans",
          'Indianapolis Colts'    => "Indianapolis Colts",
          'Jacksonville Jaguars'  => "Jacksonville Jaguars",
          'Kansas City Chiefs'    => "Kansas City Chiefs",
          'Los Angeles Rams'      => "Los Angeles Rams",
          'Miami Dolphins'        => "Miami Dolphins",
          'Minnesota Vikings'     => "Minnesota Vikings",
          'New England Patriots'  => "New England Patriots",
          'New Orleans Saints'    => "New Orleans Saints",
          'New York Giants'       => "New York Giants",
          'New York Jets'         => "New York Jets",
          'Oakland Raiders'       => "Oakland Raiders",
          'Philadelphia Eagles'   => "Philadelphia Eagles",
          'Pittsburgh Steelers'   => "Pittsburgh Steelers",
          'San Diego Chargers'    => "San Diego Chargers",
          'San Francisco 49ers'   => "San Francisco 49ers",
          'Tampa Bay Buccaneers'  => "Tampa Bay Buccaneers",
          'Tennessee Titans'      => "Tennessee Titans",
          'Washington Redskins'   => "Washington Redskins",

    ),
  
    'PLAYERPOSITIONS' => array(
        'quarterback'       => "Quarterback",
        'runningback_one'   => "Runningback One",
        'runningback_two'   => 'Runningback Two',
        'widereceiver_one'  => 'Widereceiver One',
        'widereceiver_two'  => 'Widereceiver Two',
        'tightend'          => 'Tight End',
        'defense'           => 'Defense',
    ),




);
