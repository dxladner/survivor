<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// Users
Route::get('/users','UserController@index');
Route::get('/users/{id?}', 'UserController@show');
Route::get('/users/edit/{id}', 'UserController@edit');
Route::post('/users/edit/{id}', 'UserController@update');
Route::post('/users/{id?}/delete', 'UserController@destroy');

Route::get('/users/dashboard','UserController@index');


// Admin
Route::group(['prefix' => 'admin', 'namespace' => 'admin'], function()
{
    Route::get('/dashboard','DashboardController@index');

    // Players
    Route::get('/players', 'PlayersController@index');
    Route::post('/players', 'PlayersController@store');
    Route::get('/player/{id?}/edit', 'PlayersController@edit');
    Route::post('/player/{id?}/edit', 'PlayersController@update');
    Route::post('/player/{id?}/delete', 'PlayersController@destroy');

    // Users
    Route::get('/contestants','ContestantsController@index');
    Route::get('/contestants/show', 'ContestantsController@show');
    Route::post('/contestants/create', 'ContestantsController@create');
    Route::get('/contestants/{id?}/edit', 'ContestantsController@edit');
    Route::post('/contestants/{id?}/edit', 'ContestantsController@update');
    Route::post('/contestants/{id?}/delete', 'ContestantsController@destroy');

    // CMS
    Route::get('/cms', 'CMSController@index');

    // Posts
    Route::get('/posts','PostsController@index');
    Route::post('/posts','PostsController@create');
    Route::get('/posts/{id}/edit', 'PostsController@edit');
    Route::post('/posts/{id?}/edit', 'PostsController@update');
    Route::post('/posts/{id?}/delete', 'PostsController@destroy');


});
